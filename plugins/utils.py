import os
import json


def get_expected(transformer, path):
    filepath = filepath = os.path.join(path, "output", transformer + ".json")
    with open(filepath, "r") as f:
        expected = json.loads(f.read())
    return expected


skip_azure_pdf = (
    os.environ.get("INGESTUM_AZURE_CV_ENDPOINT") is None
    or os.environ.get("INGESTUM_AZURE_CV_SUBSCRIPTION_KEY") is None
)
