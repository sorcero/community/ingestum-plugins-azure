# -*- coding: utf-8 -*-

#
# Copyright (c) 2021 Sorcero, Inc.
#
# This file is part of Sorcero's Language Intelligence platform
# (see https://www.sorcero.com).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#


import os
import pytest

from ingestum import sources
from ingestum import transformers

from plugins import utils


__path__ = os.path.dirname(os.path.realpath(__file__))


pdf_source = sources.PDF(path="plugins/azure/tests/data/test.pdf")


@pytest.mark.skipif(
    utils.skip_azure_pdf, reason="INGESTUM_AZURE_CV_* variables not found"
)
def test_pdf_source_create_text_document_ocr_azure():
    document = transformers.PDFSourceCreateTextDocumentOCR(
        first_page=1, last_page=3, engine="azure"
    ).transform(source=pdf_source)
    assert document.dict() == utils.get_expected(
        "pdf_source_create_text_document_ocr_azure", path=__path__
    )
