set -e

export INGESTUM_PLUGINS_DIR=$PWD/plugins

echo "Setting up environment..."
virtualenv env > /dev/null
source env/bin/activate > /dev/null
pip install -r requirements.txt > /dev/null
ingestum-install-plugins > /dev/null

pyflakes plugins/
black --check plugins/

for plugin in $INGESTUM_PLUGINS_DIR/*/ ; do

    echo "TESTING ${plugin}"

    test="${plugin}tests/test_plugin.py"
    if test -f "$test"; then
        echo RUN $test
        python3 -m pytest $test
    fi
done

echo "ALL OK"
